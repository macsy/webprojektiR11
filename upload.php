<?php
$target_dir = "uploads/";
$name=basename($_FILES["fileToUpload"]["name"]);
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    $m = new MongoClient();
    //echo "Connection to database successfully";
    // select a database
   $db = $m->timestamps;
   //echo "Database timestamps selected";
   $collection = $db->id;
   //echo "Collection id selected succsessfully";
   date_default_timezone_set('Europe/Helsinki');
   $document = array( 
  "id" => $name, 
  "date"=> date('m/d/Y h:i:s', time()),
  "event"=> "upload"
  );
  $collection->insert($document);
  $m->close();
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        include("indexv2.php");
    } else {
        echo "nope";
    }
}
?>