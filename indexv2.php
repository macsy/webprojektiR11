<!DOCTYPE html>
<html lang="en">
	<head>
<?php
session_start();
//Check if user is already logged in, if so direct user to main page
if(!isset($_SESSION["loggedin"])) {
    ?>
<script type="text/javascript">
    console.log("Not logged in!");
    window.location = "login.php";
</script>
<?php } ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Ryhma11 Cloud Server</title>
    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Cantarell" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<link href="//cdn.muicss.com/mui-0.9.6/css/mui.min.css" rel="stylesheet" type="text/css" />
    <script src="//cdn.muicss.com/mui-0.9.6/js/mui.min.js"></script>
    <link href="css/style.css" rel="stylesheet">
    <link href="css/fade.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="css/material.css" rel="stylesheet">
  </head>
  <body>
	<div id="appbar">
	  <div class="btn-primary">
	      <div class="mui--text-display3" id="hienosaato">Ryhmä 11 Pilvipalvelu v2</div>
	  </div>
	</div>
    <div class="mui-container-fluid text-center carousel slide carousel-fade">
	<div class="row">
		<div id="listat">
			<div class="maxsize mui-container-fluid text-center" id="padding">
				<form action="upload.php" method="post" enctype="multipart/form-data">
				<input class="btn btn-primary" type="file" name="fileToUpload" id="fileToUpload">
        		<input class="btn btn-primary" type="submit" value="Upload" name="submit">
				</form>
				<form action='logout.php'>
				<input type="submit" class="btn btn-primary logout" value="Logout" name="logout"/>
				</form>
			</div>
			    <table class="mui-table mui-table--bordered">
			        <tr>
			         <td>
			  <form class="mui-textfield" action="delete.php" method="post" name="formfilemanager">
				  <div id="filebox">
				  <ul id="filelist">
				  <?php include("loadfiles.php");?>
				  </ul>
				  </div>
			  </form>
			          </td>
			          <td>
			<ul class="list-unstyled" id="kannykkaversio">
                <?php
                  $m = new MongoClient();
                  $db = $m->timestamps;
                  $collection = $db->id;
                  $cursor = $collection->find();
                   foreach ($cursor as $document) {
                      echo '<li>'.$document["id"] .'  '. $document["date"] .'  '.$document["event"]."<li>";
                   }
                   $m->close();
                ?>
			</ul>
			</td>
		</tr>
		</table>
		</div>
	</div>
</div>
  </body>
</html>