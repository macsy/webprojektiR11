<?php
function scan_dir($dir) {
    $ignored = array('.', '..', '.svn', '.htaccess');
    $files = array();    
    foreach (scandir($dir) as $file) {
        if (in_array($file, $ignored)) continue;
        $files[$file] = filemtime($dir . '/' . $file);
    }
    arsort($files);
    $files = array_keys($files);
    return ($files) ? $files : false;
}

//$id=0;
// make connection
//$connection = new MongoClient();
// select database
//$db = $connection->timespamps;
// select collection
//$collection = $db->ids;


$dire="uploads/";
$pic =  array('gif','png','jpg');
$mov =  array('webm','mp4','mov');
$sound =  array('mp3','ogg','wav');
$farray = scan_dir($dire);
if(!empty($farray)){
    foreach ($farray as &$filename){
            $ext = pathinfo($filename);            
            $ext = strtolower($ext['extension']);
            $name = basename($filename, '.' . $ext);
            $name = preg_replace('/\s+/', '-', $name);
            if (strlen($filename)>80){
                $fileconcat = substr($filename, 0, 70) . '(..).' . $ext;
            }
            else{$fileconcat = $filename;}
            if(in_array($ext,$pic) ){
        		?>
                <li><a style="color: white;" class="rollover" id='li-<?php echo $name?>-<?php echo $ext?>' href="uploads/<?php echo $filename;?>" ><?php echo $fileconcat?><span><img class="preimg hidden" alt="image" src="uploads/<?php echo $filename; ?>"/></span></a><a class=tab></a><input type="submit" style="color: black;" id='del' name="<?php echo $filename?>" value="delete"/></li> 
                <?php
            }
            elseif(in_array($ext,$mov) ){
                ?>
                <li><a style="color: white;" class="rollover" align="center" id='li-<?php echo $name?>-<?php echo $ext?>' href="uploads/<?php echo $filename; ?>" ><?php echo $fileconcat?><span><video muted id='mov-<?php echo $name?>-<?php echo $ext?>' loop class="preimg vid" height="400"><source src="uploads/<?php echo $filename; ?>" type="video/mp4"></video></span></a><a class=tab></a><input type="submit" style="color: black;" id='del' name="<?php echo $filename?>" value="delete"/></li> 
                <?php
            }
            elseif(in_array($ext,$sound) ){
                ?>
                <li><a style="color: white;" class="rollover"id='li-<?php echo $name?>-<?php echo $ext?>' href="uploads/<?php echo $filename; ?>" ><?php echo $fileconcat?><span><audio muted id='aud-<?php echo $name?>-<?php echo $ext?>' loop class="preimg aud"><source src="uploads/<?php echo $filename; ?>" type="audio/mp3"></audio></span></a><a class=tab></a><input type="submit" style="color: black;" id='del' name="<?php echo $filename?>" value="delete"/></li> 
                <?php
            }
            else{  
                ?>
                <li><a style="color: white;" class="rollover" id='li-<?php echo $name?>-<?php echo $ext?>' href="uploads/<?php echo $filename; ?>" ><?php echo $fileconcat?></a><a class=tab></a><input type="submit" style="color: black;" class='del'  name="<?php echo $filename?>" value="delete"/></li> 
                <?php
            }
    unset($filename);
    }
}
  
?>