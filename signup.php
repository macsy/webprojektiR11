<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Ryhma11 Cloud Server</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Cantarell" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<link href="//cdn.muicss.com/mui-0.9.6/css/mui.min.css" rel="stylesheet" type="text/css" />
    <script src="//cdn.muicss.com/mui-0.9.6/js/mui.min.js"></script>
    <link href="css/style.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="css/material.css" rel="stylesheet">
  </head>
<body>
	<div id="appbar">
	  <div class="btn-primary">
        <div class="mui--text-display3" id="hienosaato">Signup</div>
    </div>
</div>
	
	<div class="mui-container-fluid text-center">
        <form name="myForm" action="createuser.php" method="post">
            <h2 class="scalablelogintext" style="color: white;">Username:</h2>
            <br>
            <div class="toppadsmall">
            <input class="scalableinput btn btn-primary" type="text" name="newname" required/> </div>
            <br>
            <h2 class="scalablelogintext" id="omapadding" pattern="[A-Za-z0-9]*" style="color: white;">Password:</h2>
            <br>
            <div class="toppadsmall">
            <input class="scalableinput btn btn-primary" pattern="[A-Za-z0-9]*" type="password" name="newpassword" required/></div>
            <br>
            <div class="toppadlarge">
            <input class="scalablebutton btn btn-primary" type="submit" name="createUser" value="Create user" onclick="toLogin()"/> </div>
        </form>
        
        <!--Navigate to login page-->
        <script type="text/javascript">
            function toLogin() {
                window.location = "login.php";
            }
        </script>
        
        <input id="backtologin" class="btn btn-primary" type="button" onclick="toLogin()" value=" Back to login page">
    </div>
</body>
</html>
