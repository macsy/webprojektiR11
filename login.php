<!DOCTYPE html>
<head>
<?php
session_start();

//Check if user is already logged in, if so direct user to main page
if(isset($_SESSION["loggedin"])) {
    ?>
<script type="text/javascript">
    console.log("Already logged in, directing to the main page")
    window.location = "indexv2.php";
</script>

<?php 
} else {
    //Check if user user tries to login, used in testing
    if(isset($_POST['login'])) {
        $postedUsername = $_POST['username'];
        $postedPassword = $_POST['password'];
        
        //Check if user exists in the database, if so direct user to main page
        $m = new MongoClient();
        $db = $m->users;
        $collection = $db->id;
        $cursor = $collection->find();
        foreach ($cursor as $document) {
          if($document["username"] == $postedUsername && $document["password"] == $postedPassword){
            $_SESSION['loggedin'] = true;
            $_SESSION['username'] = $postedUsername;
            $m->close();
            ?>
            <script>
                console.log("Username and password correct, login")
                window.location = "indexv2.php"
            </script>
            <?php
          }
        }
        $m->close();
        
/*
        if($postedUsername == 'asd'){
            $_SESSION['loggedin'] = true;
            $_SESSION['username'] = $postedUsername;
            ?>
            <script>
                console.log("Username checked and approved, login");
                window.location = "testlogin.php";
            </script>
            <?php
        } else{
            ?> <script>
                console.log("Username did not match");
            </script>
            <?php
        }
    ?>
 
    <?php */
    } else {
        ?>
        <script type="text/javascript">
            console.log("Username has not been set");
        </script>
        <?php
    }
}
    ?>



    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Ryhma11 Cloud Server</title>
    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Cantarell" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<link href="//cdn.muicss.com/mui-0.9.6/css/mui.min.css" rel="stylesheet" type="text/css" />
    <script src="//cdn.muicss.com/mui-0.9.6/js/mui.min.js"></script>
    <link href="css/style.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="css/material.css" rel="stylesheet">
</head>
<body>

	<div id="appbar">
	  <div class="btn-primary">
	      <div class="mui--text-display3" id="hienosaato">Login Page</div>
	  </div>
	</div>
<div class="mui-container-fluid text-center" id="padding">
<form name="myForm" action="login.php" method="post">
    <input class="scalableinput btn btn-primary" type="text" name="username" pattern="[A-Za-z0-9]*" id="uname" placeholder="username" required/>
    <br>
    <br>
    <input class="scalableinput btn btn-primary" type="password" pattern="[A-Za-z0-9]*" name="password" id="pw" placeholder="password" required/>
    <br>
    <div id="login"><input class="scalablelogin btn btn-primary" type="submit" name="login" id="loginbutton" value="Login"/></div>
</form>

<script type="text/javascript">
    function toSignup() {
        window.location = "signup.php";
    }
</script>

<input id="signup" class="scalablesignup btn btn-primary" type="button" onclick="toSignup()" value="Sign up">
</div>

</body>
</html>
